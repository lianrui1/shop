package com.tianjue.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TulingmallAuthcenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(TulingmallAuthcenterApplication.class, args);
	}

}
